package cn.com.wayne.lang.reflect;

public class Client1 {
    public static void main(String[] args) {
        //创建代理对象
        AccountAdvice1 proxy = new AccountAdvice1();
        IAccountService iAccountService = (IAccountService)proxy.bind(new AccountServiceImpl());
        iAccountService.transfer();
    }
}
