package cn.com.wayne.springframework.aop;

import net.sf.cglib.proxy.Enhancer;
import net.sf.cglib.proxy.MethodInterceptor;

/**
 * 代理工厂类
 * <p>
 * 定义一个工厂类来创建代理, 其实不创建这个类也行, 但为了模仿Spring还是创建了.
 * @SuppressWarnings是为了抑制警告, 就是编译器上面的黄线.
 */
public class ProxyFactory {
    @SuppressWarnings("unchecked")
    public static <T> T createProxy(final Class<?> targetClass, final MethodInterceptor methodInterceptor) {
        return (T) Enhancer.create(targetClass,methodInterceptor);
    }
}
