package cn.com.wayne.springframework.aop;

public interface IAccountService {
    //主业务逻辑: 转账
    void transfer();
}
