package cn.com.wayne.springframework.aop;

import java.lang.reflect.Method;

/**
 * 切面类
 * <p>
 * 创建一个切面类, 类中配置切入点和增强.
 */
public class AccountAspect extends BaseAspect {

    /**
     * 切入点
     */
    public boolean isIntercept(Method method, Object[] args) throws Throwable {
        return method.getName().equals("transfer");
    }

    /**
     * 前置增强
     */
    public void before() throws Throwable {
        System.out.println("对转账人身份进行验证.");
    }
}
