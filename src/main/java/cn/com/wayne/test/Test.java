package cn.com.wayne.test;

public class Test {

    long width;

    public Test(long width) {
        this.width = width;
    }

    public static void main(String[] args) {
        Test a,b,c;
        a = new Test(42L);
        b = new Test(42L);
        c = b;
        long s = 42L;

        System.out.println(b==c);
        System.out.println(a.equals(s));
        System.out.println(a==b);
    }
}
